import { createRoot } from 'react-dom/client'
import './styles/base.css'
import './styles/index.css'
import App from './App'
import { Provider } from 'mobx-react'
import RootStore from './store'

createRoot(document.querySelector('#root')).render(
  <Provider {...new RootStore()}>
    <App />
  </Provider>
)
